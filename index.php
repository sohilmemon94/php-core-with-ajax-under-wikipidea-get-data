<!DOCTYPE html>
<html>
   <head>
      <title>Practical Ajax</title>
      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      
      
      <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
      <script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
      <script type="text/javascript">
         var url = "http://localhost:8000/";         
      </script>
      <script src="./js/custom.js"></script>
     
      <script type="text/javascript">
         $(document).ready( function () {
             
             var table =  $('#table_id').DataTable();
         
         });
      </script>
   </head>
   <body>
      <div class="container">
         <div class="row">
            <div class="col-lg-12 margin-tb">
               <div class="pull-left">
                  <h2>Practical Ajax</h2>
               </div>
               <div class="pull-right">
               
                  <button id="multidelete" class="btn btn-danger">Selected Delete</button>
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-item">
                  New City
                  </button>
               </div>
            </div>
         </div>
         <table id="table_id" class="display">
            <thead>
               <tr>
                  <th><input type="checkbox" id="allcheck"  data-target="#alldelete-item"></th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>                               
            </tbody>
         </table>
         <div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                     <h4 class="modal-title" id="myModalLabel">Create City</h4>
                  </div>
                  
                  <div class="modal-body">                   
                            <label class="control-label" for="title">City Name:</label>
                            <input type="text" class="searchbycity" placeholder="Search By City Name.." name="search">
                            <button type="submit" class="btn crud-search btn-success">Search</button>                    
                </div>
                <hr/>
                  <div class="modal-body">
                     <form data-toggle="validator" action="crud/create.php" method="POST" class="form" id="addForm">
                        <div class="form-group">
                           <label class="control-label" for="title">Title:</label>
                           <input type="text" name="title" class="form-control" data-error="Please enter title." required />
                           <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="title">Description:</label>
                           <textarea name="extract" class="form-control" id="editor" rows="10" cols="80"  data-error="Please enter description." required></textarea>
                           <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn crud-submit btn-success">Submit</button>
                        </div>
                        <script>
        
    CKEDITOR.replace('extract');
</script>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                     <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                  </div>
                  
                  <div class="modal-body">
                     <form data-toggle="validator" action="crud/update.php" method="POST" class="updateform">
                        <input type="hidden" name="id" class="edit-id">
                        <div class="form-group">
                           <label class="control-label" for="title">Title:</label>
                           <input type="text" name="title" class="form-control" data-error="Please enter title." required />
                           <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="title">Description:</label>
                           <textarea name="extract" class="form-control" id="editor1" rows="10" cols="80"  data-error="Please enter description." required></textarea>
                           <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>
                     
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
   </body>
</html>