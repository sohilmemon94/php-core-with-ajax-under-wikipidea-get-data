$(document).ready(function() {
    var modal = $(".modal");
    var form = $(".form");
    var updateForm = $(".updateform");
    var btnSave = $(".crud-submit"),
        btnUpdate = $(".crud-submit-edit"),
        btnDelete = $(".remove-item"),
        btnSearch = $(".crud-search");

    $('#create-item').on('hidden.bs.modal', function() {
        $(this).find('form').trigger('reset');
    });

    $('#create-item').on('hidden.bs.modal', function(e) {
        $(this)
            .find("input,textarea, .searchbycity")
            .val('')
            .end()
            .find("input[type=text]")
            .prop("checked", "")
            .end();

    })

    $.fn.dataTable.ext.errMode = function(obj, param, err) {
        var tableId = obj.sTableId;
        console.log(obj);
    };
    var table = $("#table_id").DataTable({

        "ajax": 'crud/getData.php',
        dataSrc: 'data',
        columns: [
            { data: "check" },
            { data: "title" },
            { data: "listextract" },
            { data: "action" },
        ],
    });

    btnSave.click(function(e) {
        e.preventDefault();
        var form_action = $("#create-item").find("form").attr("action");
        var title = form.find('input[name="title"]').val();
        var extract = CKEDITOR.instances.editor.getData();
        if (title != "" && extract != "") {
            $.ajax({
                type: "POST",
                url: form_action,
                data: { title: title, extract: extract },
                success: function() {
                    table.ajax.reload();
                    form.trigger("reset");
                    $(".form").trigger('reset');
                    modal.modal("hide");
                    toastr.success("City Added Successfully.", "Success Alert", {
                        timeOut: 5000,

                    });
                },
                error: function() {
                    alert("You are missing title or extract.");
                },
            });

            CKEDITOR.instances.editor.setData();
        }
    });

    $(document).on("click", ".edit-item", function() {
        modal.find('button[type="submit"]').text("Update");

        var rowData = table.row($(this).parents("tr")).data();

        updateForm.find('input[name="id"]').val(rowData.id);
        updateForm.find('input[name="title"]').val(rowData.title);
        updateForm.find('textarea[name="extract"]').text(rowData.extract);
        //CKEDITOR.instances.editor1.insertHtml(rowData.extract);
    });

    btnUpdate.click(function(e) {
        e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var formData = updateForm.serialize();

        $.ajax({
            type: "POST",
            url: form_action,
            data: formData,
            success: function() {
                table.ajax.reload();
                updateForm.trigger("reset");
                modal.modal("hide");
                toastr.success("City Updated Successfully.", "Success Alert", {
                    timeOut: 5000,
                });
            },
            error: function() {
                alert("You are missing title or extract.");
            },
        });
    });
    // btnDelete.click(function() {

    //     var rowData = table.row($(this).parents("tr")).data();
    //     var form_action = "crud/delete.php";
    //     if (!confirm("Are you sure?")) return;

    //     var rowid = rowData.id;
    //     console.log(rowid);
    //     if (!rowid) return;

    //     $.ajax({
    //         type: "POST",
    //         dataType: "JSON",
    //         url: form_action,
    //         data: { id: rowid },
    //         success: function() {
    //             console.log("delete");
    //             table.ajax.reload();
    //             toastr.success("City Deleted Successfully.", "Success Alert", {
    //                 timeOut: 5000,
    //             });
    //         },
    //     });
    // });

    // $(document).on("click", ".remove-item", function() {

    // });

    btnSearch.click(function(e) {
        e.preventDefault();
        var val = $(".searchbycity").val();
        if (val != "") {
            $.ajax({
                url: "https://de.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=" +
                    val,
                type: "GET",
                cors: true,
                contentType: "jsonp",
                dataType: "jsonp",
                secure: false,
                headers: {
                    "Access-Control-Allow-Origin": "*",
                },

                success: function(data) {
                    $.each(data.query.pages, function(i, item) {
                        form.find('input[name="title"]').val(item.title);
                        CKEDITOR.instances.editor.insertHtml(item.extract);
                    });
                },
                error: function() {
                    alert("You are missing city Name.");
                },
            });
        }
    });

    $("#allcheck").click(function() {
        $("#table_id input[type=checkbox]").not(this).prop("checked", this.checked);
    });

    $("#multidelete").click(function() {
        var all_arr = [];

        $("#table_id #rowcheck:checked").each(function() {
            var rowDatadelete = table.row($(this).parents("tr")).data();
            if (jQuery(this).is(":checked")) {
                var id = rowDatadelete.id;
                var splitid = id.split(",");
                var postid = splitid[0];

                all_arr.push(postid);
                // console.log(all_arr);
            }
        });

        if (all_arr.length > 0) {
            var isDelete = confirm("Do you really want to delete records?");
            if (isDelete == true) {

                $.ajax({
                    url: "crud/multidelete.php",
                    type: "POST",
                    data: { id: all_arr },

                    success: function(response) {
                        console.log("delete");
                        //table.row($(this).parents("tr")).remove().draw();
                        setInterval(function() {
                            table.ajax.reload();

                            // $('#table_id').dataTable().ajax.reload();
                        }, 500);
                        $.each(all_arr, function(i, l) {
                            $("#tr_" + l).remove();
                        });
                        toastr.success("City Deleted Successfully.", "Success Alert", {
                            timeOut: 5000,
                        });
                        //table.ajax.reload();



                    },
                });
            }
        }
    });
});